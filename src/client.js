const common = require('postchain-common');
const Postchain = require('postchain-client');
const util = common.util;
const serialization = common.serialization;

const ACCOUNT_TYPE_SIMPLE = 1;
const ACCOUNT_TYPE_MULTI_SIG = 2;

class FTTransaction {
    constructor (signers, gtxClient, restClient) {
        this.rq = gtxClient.newRequest(signers);
        this.gtxClient = gtxClient;
        this.txID = null;
        this.restClient = restClient;
    }

    register (accountDesc) {
        if (!Buffer.isBuffer(accountDesc)) throw Error("AccountDESC should be a Buffer");
        this.rq.ft_register(accountDesc);
    }

    issue (assetID, amount, accountID) {
        this.rq.ft_issue(assetID, amount, accountID);
    }

    send (params) {
        const from = params.from;
        const to = params.to;
        const amount = params.amount;
        const assetID = params.assetID;
        this.rq.ft_xfer([[from, assetID, amount]],[[to, assetID, amount]]);
    }

    xfer (inputs, outputs) {
        this.rq.ft_xfer(inputs, outputs);
    }

    rawCall (name, ...args) {
        this.rq.addCall(name, ...args);
    }

    getBufferForSigning () {
        return this.rq.getBufferToSign();
    }

    getDigestForSigning () {
        const buffer = this.rq.getBufferToSign();
        return util.hash256(buffer);
    }

    getTxID () {
        if (this.rq.gtxBytes) {
            return util.hash256(this.rq.gtxBytes)
        } else {
            throw Error("TxID is only available after submit()")
        }
    }

    sign (privkey, pubKey) {
        this.rq.sign(privkey, pubKey);
    }

    addSignature (pubkey, signature) {
        this.rq.addSignature(pubkey, signature);
    }

    submit () {
        return new Promise( (resolve, reject) => {
            this.rq.send( (error) => {
                if (error) reject(error);
                else {
                    this.txID = this.getTxID();
                    resolve(this.txID);
                }
            });
        });
    }

    submitAndWaitConfirmation () {
        return this.submit().then( txid => this._waitConfirmation(txid) );
    }

    _waitConfirmation(txid) {
        return new Promise( (resolve, reject) => {
            setTimeout(() => {
                console.log("Polling ", txid.toString('hex'));
                this.restClient.status(txid, (err, res) => {
                    console.log("Got status response:", err, res);
                    if (err) reject(err);
                    else {
                        const {status} = res;
                        switch (status) {
                            case "confirmed": resolve(txid); break;
                            case "rejected": reject(Error("Message was rejected")); break;
                            case "unknown": reject(Error("Server lost our message")); break;
                            case "waiting":
                                this._waitConfirmation(txid).then(resolve, reject);
                                break;
                            default:
                                console.log(status);
                                reject(Error("got unexpected response from server"));
                        }
                    }
                });
            }, 511);
        });
    }
}

class FTClient {

    constructor (restClientURL) {
        let restClient = null;
        if (typeof restClientURL === 'string') {
            restClient = Postchain.restClient.createRestClient(restClientURL);
        } else if (typeof restClient === 'object') {
            restClient = restClientURL;
        } else throw Error("Wrong argument type");

        this.restClient = restClient;

        this.gtxClient = Postchain.gtxClient.createClient(
            restClient,
            ["ft_xfer", "ft_issue", "ft_register"]
        );
    }

    makeAccountDesc (data) {
        return serialization.encodeArg(data)
    }

    makeSimpleAccountDesc(pubkey) {
        checkPublicKey(pubkey, 'pubkey');
        return this.makeAccountDesc([ACCOUNT_TYPE_SIMPLE, [], pubkey]);
    }

    makeMultSigAccountDesc(thresh, pubkeys) {
        pubkeys.map(pubkey => checkPublicKey(pubkey, 'pubkey'));
        return this.makeAccountDesc([ACCOUNT_TYPE_MULTI_SIG, [], thresh, pubkeys]);
    }

    makeAccountID (desc) {
        return util.hash256(desc);
    }

    makeTransaction (signers) {
        return new FTTransaction(signers, this.gtxClient, this.restClient);
    }

    getBalance (accountID, assetID) {
        return new Promise( (resolve, reject) => {
            this.restClient.query(
                {type:'ft_balance', account_id: accountID, asset_id: assetID},
                (error, result) => {
                    if (error) reject(error);
                    else {
                        resolve(result.balance);
                    }
                }
            );
        });
    }

    getHistory (accountID, assetID) {
        return new Promise( (resolve, reject) => {
            this.restClient.query(
                {type:'ft_history', account_id: accountID, asset_id: assetID},
                (error, result) => {
                    if (error) reject(error);
                    else {
                        resolve(result);
                    }
                }
            );
        });
    }

}

function checkPublicKey(pubKey, pubKeyName) {
    if (!pubKey || !Buffer.isBuffer(pubKey) || pubKey.length !== 33) {
        console.error(pubKey);
        console.log(pubKey.length);
        throw new Error(`${pubKeyName} is not a valid pubKey:`);
    }
}

module.exports = FTClient;
